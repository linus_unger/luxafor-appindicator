#!/usr/bin/python

import os
import json
from collections import OrderedDict	

def get_path(f):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), f)

def get_colors():
    return json.load(open(get_path('../config/colors.json')), object_pairs_hook=OrderedDict)