#!/usr/bin/python

import os
import json
import time
import config
from switch import Switch

class Party:

    def __init__(self):
	self.running = False
	self.colors = self.party_colors()
	self.switch = Switch()

    def start(self):
	self.running = True
	pos = 0
	while self.running:
	    self.switch.change(self.colors.keys()[pos])
	    pos = pos + 1
	    if (pos >= len(self.colors.keys())):
		pos = 0

	    time.sleep(.2)

    def stop(self):
	self.running = False

    def party_colors(self):
	colors = config.get_colors()
	del colors['off']
	return colors

if __name__ == '__main__':
    try:
	p = Party()
	p.start()
    except KeyboardInterrupt:
	p.stop()
	exit()
