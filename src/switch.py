#!/usr/bin/python

import os
import json
import argparse
import usb.core
import config

class Switch:

	def __init__(self):
		self.colors = config.get_colors()
		self.setup_usb()

	def setup_usb(self):
		self.dev = usb.core.find(idVendor=0x04d8, idProduct=0xf372)

		if self.dev is None:
			print('Not connected')
			return

		try:
			self.dev.detach_kernel_driver(0)
		except usb.core.USBError:
			pass

		try:
			self.dev.set_configuration()
		except usb.core.USBError:
			print("Did you give Luxafor USB permission to your user?")
			return

		self.dev.set_configuration()

	def change(self, color):
		self.dev.write(1, [0, 0])
		self.dev.write(1, [0, self.colors[color]['code']])

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Change Luxafor color')
	parser.add_argument('color', choices=config.get_colors(), help='color to change to')
	args = parser.parse_args()

	s = Switch()
	s.change(args.color)
