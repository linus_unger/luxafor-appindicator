#!/usr/bin/python

import gi
gi.require_version('AppIndicator3', '0.1')
gi.require_version('Gtk', '3.0')

import sys
import os
import json
import signal
import subprocess
from collections import OrderedDict																	
from subprocess import call
from gi.repository import Gtk as gtk, AppIndicator3 as appindicator
import config

class AppIndicator:

    def __init__(self):
	self.party_pro = None
	self.indicator = appindicator.Indicator.new('luxafortray', config.get_path('../img/icon.svg'), appindicator.IndicatorCategory.SYSTEM_SERVICES)
	self.indicator.set_title('Luxafor Appindicator')
	self.indicator.set_status(appindicator.IndicatorStatus.ACTIVE)
	self.indicator.set_menu(self.menu())

    def add_menu(self, title, color):
	item = gtk.ImageMenuItem(title)
	item.connect('activate', self.switch_led, color)
	img = gtk.Image()
	img.set_from_file(config.get_path('../img/colors/' + color + '.svg'))
	item.set_image(img)
	return item

    def menu(self):
	menu = gtk.Menu()
	colors = config.get_colors()
	for k, v in colors.iteritems():
	    menu.append(self.add_menu(v['status'], k))

	menu.append(self.add_menu('Party mode', 'party'))
	sep = gtk.SeparatorMenuItem()
	menu.append(sep)
	exittray = gtk.ImageMenuItem('Quit')
	exittray.connect('activate', self.quit)
	menu.append(exittray)

	menu.show_all()
	return menu

    def switch_led(self, _, color):
	self.kill_party()
	if (color == 'party'):
	    self.start_party()
	    return
	subprocess.Popen(['python', config.get_path('switch.py'), color], shell=False)

    def start_party(self):
	cmd = ['python', config.get_path('party.py')]
	self.party_pro = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=False, preexec_fn=os.setsid)

    def kill_party(self):
	if (self.party_pro == None):
	    return
	os.killpg(os.getpgid(self.party_pro.pid), signal.SIGTERM)
	self.party_pro = None

    def quit(self, _):
	self.switch_led(_, 'off')
	gtk.main_quit()

if __name__ == "__main__":
    AppIndicator()
    gtk.main()
