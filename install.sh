# /usr/bin/bash

sudo apt update
sudo apt install gir1.2-appindicator3 python-pip -y

pip install pyusb

read -p "Add usb rules for your user (y/n): " install_rules
if [[ $install_rules == 'y' ]] ; then
	sudo bash -c ' echo "SUBSYSTEM==\"usb\", ATTR{idVendor}==\"04d8\", ATTR{idProduct}==\"f372\" MODE=\"0664\", OWNER=\"$SUDO_USER\"" > /etc/udev/rules.d/luxafor.rules'
	sudo udevadm control --reload
	sudo udevadm trigger
fi

install_path=$(pwd)
cronfile="$install_path/src/tray.py"

read -p "Place application in system folder (y/n): " install_move
if [[ $install_move == 'y' ]] ; then
	read -p "Change install folder, leave blank for (/usr/lib/luxafor-appindicator): " install_path
	if [[ $install_path == '' ]] ; then
		install_path="/usr/lib/luxafor-appindicator"
	fi
	sudo cp -R $(pwd) $install_path
	sudo rm -f $install_path/install.sh
	sudo chmod -R 755 $install_path
	sudo chmod +x $install_path/src/tray.py
	sudo chmod +x $install_path/src/switch.py
	sudo rm -f /usr/bin/luxafor-tray
	sudo rm -f /usr/bin/luxafor-switch
	sudo ln -s $install_path/src/tray.py /usr/bin/luxafor-tray
	sudo ln -s $install_path/src/switch.py /usr/bin/luxafor-switch
	cronfile="/usr/bin/luxafor-tray"
	echo "Copied application to: $install_path"
fi

read -p "Autostart on system boot (y/n): " install_cron
if [[ $install_cron == 'y' ]] ; then
	mycron="$(crontab -l)"
	if [[ $mycron != *"$cronfile"* ]] ; then
		echo 'Installing cronjob'
		crontab -l | { cat; echo "@reboot nohup $(which python) $cronfile &"; } | crontab -
	fi
fi

if [[ $install_move == 'y' ]] ; then
	read -p "Delete setup files in $(pwd) (y/n): " install_delete
	if [[ $install_delete == 'y' ]] ; then
		sudo rm -R $(pwd)
	fi
fi

read -p "Start appindicator now (y/n): " install_start
if [[ $install_start == 'y' ]] ; then
	nohup $(which python) $cronfile &
fi

echo 'Install complete'
