# Luxafor Appindicator

![Luxafor Appindicator](img/icon.svg)


## Automatic installation (Recommended)

```bash
bash ./install.sh
```


## Manual installation

We’ll need the appindicator3 and python-pip package installed.


### Ubuntu/Mint/Debian

```bash
sudo apt install gir1.2-appindicator3 python-pip -y
```

###Fedora

```bash
sudo dnf install libappindicator-gtk3 python-pip -y
```

For other distributions, just search for any packages containing `appindicator`.


### You need PyUSB installed

```bash
pip install pyusb
```

### Add usb rules for your user

It’d be really annoying to have to sudo every light control though, so we’ll open this one particular device to your user.

```bash
sudo bash -c ' echo "SUBSYSTEM==\"usb\", ATTR{idVendor}==\"04d8\", ATTR{idProduct}==\"f372\" MODE=\"0664\", OWNER=\"$SUDO_USER\"" > /etc/udev/rules.d/luxafor.rules'
```


#### Reload udev with

```bash
sudo udevadm control --reload
sudo udevadm trigger
```


### Autostart on system boot

We want our system tray indicator to start automatically on boot, we don’t want to run it manually each time. To do that, simply add the following command to your crontab.

```bash
@reboot nohup python [luxafor directory]/src/tray.py &
```


## Script functions

### Manually switch color

If you used the automatic install and choosed "Place application in system folder", then luxafor-switch will be available on your system.

```bash
luxafor-switch green
```

Else you can use the ./src/switch.py

```bash
python [luxafor directory]/src/switch.py green
```

#### Available colors are:

![Green](img/colors/green.svg) ![Red](img/colors/red.svg) ![Yellow](img/colors/yellow.svg) ![Purple](img/colors/purple.svg) ![Blue](img/colors/blue.svg) ![Turquoise](img/colors/turquoise.svg) ![White](img/colors/white.svg)


### Manually start system tray

If you used the automatic install and choosed "Place application in system folder", then luxafor-tray will be available on your system.

```bash
nohup luxafor-tray &
```

Else you can use the ./src/tray.py

```bash
nohup python [luxafor directory]/src/tray.py &
```